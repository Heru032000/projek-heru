<?php
 
use App\Data;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
 
Route::get('/home', 'HomeController@index')->name('home'); 
Route::get('/', 'HomeController@index'); 

Route::group(['prefix' => 'admin' , 'middleware' => 'admin'], function(){
    Route::Resource('/barang', 'BarangController');
    Route::Resource('/jenis', 'JenisController');
    Route::Resource('/ruang', 'RuangController');
    Route::Resource('/users', 'UserController');

    Route::get('/laporan/trs', 'LaporanController@transaksi');
    Route::get('/laporan/trs/pdf', 'LaporanController@transaksiPdf');
    Route::get('/laporan/trs/excel', 'LaporanController@transaksiExcel');

    Route::get('/laporan/barang', 'LaporanController@barang');
    Route::get('/laporan/barang/pdf', 'LaporanController@barangPdf');
    Route::get('/laporan/barang/excel', 'LaporanController@barangExcel');
});

Route::group(['prefix' => 'operator' , 'middleware' => 'operator'], function(){
    Route::get('/', 'OperatorController@index');
});

Route::Resource('/dashboard','DashboardController');
Route::Resource('/transaksi', 'TransaksiController');

Auth::routes();