<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventaris extends Model
{
    protected $table="inventaris";
    public $guarded = [];
    public $timestamps = true;

    public function transaksi()
    {
    	return $this->hasMany(Transaksi::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class,'pegawai_id');
    }

    public function jenis()
    {
    	return $this->belongsTo(Jenis::class,'id_jenis');
    }

    public function ruang()
    {
    	return $this->belongsTo(Ruang::class,'id_ruang');
    }
}
