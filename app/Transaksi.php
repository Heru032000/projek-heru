<?php

namespace App;
  
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'transaksi';
    protected $fillable = ['kode_transaksi', 'pegawai_id', 'barang_id', 'tgl_pinjam', 'tgl_kembali', 'status', 'ket'];

    public function User()
    {
    	return $this->belongsTo(User::class,'pegawai_id');
    }
    public function inventaris()
    {
    	return $this->belongsTo(Inventaris::class,'barang_id');
    }

}
