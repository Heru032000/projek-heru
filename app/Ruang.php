<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruang extends Model
{
    protected $table="ruang";
    public $guarded = [];
    public $timestamps = false;

    public function inventaris()
    {
    	return $this->belongsTo(Inventaris::class);
    }
}
