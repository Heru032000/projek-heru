<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jenis extends Model
{
    protected $table="jenis";
    public $guarded = [];
    public $timestamps = false;

    public function inventaris()
    {
    	return $this->belongsTo(Inventaris::class);
    }
}

