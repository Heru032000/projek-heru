<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ruang;

class RuangController extends Controller
{
    protected $page = 'ruang';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ruang = Ruang::all();
        $no = 1;
        $page = $this->page;
        return view($this->page.'/index',compact('page','ruang','no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = $this->page;
        return view($this->page.'/create',compact('page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
        
        if(count($request->nama_ruang) > 0)
        {
            foreach($request->nama_ruang as $item=>$v){
                $data2=array(
                    'nama_ruang'=>$request->nama_ruang[$item],
                    'kode_ruang'=>$request->kode_ruang[$item],
                    'keterangan'=>$request->keterangan[$item],
                );
                Ruang::insert($data2);
            }
        }
        return redirect('/admin/ruang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ruang = Ruang::find($id);
        $page = $this->page;
        return view($this->page.'/edit',compact('ruang','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Ruang::whereId($id)->update([
            'nama_ruang' => $request->nama_ruang,
            'kode_ruang' => $request->kode_ruang,
            'keterangan' => $request->keterangan,
        ]);

        return redirect('/admin/'.$this->page);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            
        Ruang::destroy($id);
        return redirect('/admin/ruang');
    }
}
