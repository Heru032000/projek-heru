<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Inventaris;
Use App\jenis;
Use App\Ruang;
Use App\User;

class BarangController extends Controller
{

    protected $page = 'barang';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $barang = Inventaris::all();
        $no = 1;
        $page=$this->page;
        return view($this->page.'/index',compact('barang','no','page'));
    }

    /** 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::get();
        $ruangs = Ruang::get();
        $jeniss = Jenis::get();        

        $page = $this->page;
        return view($this->page.'/create',compact('page','users','ruangs','jeniss')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
        
        if(count($request->nama) > 0)
        {
            foreach($request->nama as $item=>$v){
                $data2=array(
                    'nama'=>$request->nama[$item],
                    'kode_inventaris'=>$request->kode_inventaris[$item],
                    'kondisi'=>$request->kondisi[$item],
                    'keterangan'=>$request->keterangan[$item],
                    'jumlah'=>$request->jumlah[$item],
                    'id_jenis'=>$request->id_jenis[$item],
                    'id_ruang'=>$request->id_ruang[$item],
                    'pegawai_id'=>$request->pegawai_id[$item],
                );
                Inventaris::insert($data2);
            }
        }
        return redirect('/admin/barang');
    }

    /**
     * Display the specified resource.
     * 
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $users = User::get();
        $barang = Inventaris::find($id);
        $page = $this->page;
        return view($this->page.'/edit',compact('barang','page','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Inventaris::whereId($id)->update([
            'nama' => $request->nama,
            'kode_inventaris' => $request->kode_inventaris,
            'kondisi' => $request->kondisi,
            'keterangan' => $request->keterangan,
            'jumlah' => $request->jumlah,
            'id_jenis' => $request->id_jenis,
            'id_ruang' => $request->id_ruang,
            'pegawai_id' => $request->pegawai_id,
        ]);

        return redirect('/admin/'.$this->page);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Inventaris::destroy($id);
        return redirect('admin/barang');
    }
}
