<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;
use App\Inventaris;
use App\User; 
use Auth;

class TransaksiController extends Controller
{

    protected $page = 'transaksi';

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $page=$this->page;
        $no = 1;

        if(Auth::user()->role == '3')
        {
            $datas = Transaksi::where('pegawai_id', Auth::user()->id)->get();
        } else {
            $datas = Transaksi::get();
        }
        return view($this->page.'/index',compact('no','page','datas'));    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $getRow = Transaksi::orderBy('id', 'DESC')->get();
        $rowCount = $getRow->count();
        
        $lastId = $getRow->first();
        
        $kode = "TR00001";
        
        if ($rowCount > 0) {
            if ($lastId->id < 9) {
                $kode = "TR0000".''.($lastId->id + 1);
            } else if ($lastId->id < 99) {
                $kode = "TR000".''.($lastId->id + 1);
            } else if ($lastId->id < 999) {
                $kode = "TR00".''.($lastId->id + 1);
            } else if ($lastId->id < 9999) {
                $kode = "TR0".''.($lastId->id + 1);
            } else {
                $kode = "TR".''.($lastId->id + 1);
            }
        }
        
        $barangs = Inventaris::where('jumlah', '>', 0)->get();
        $users = User::get();
        return view('transaksi.create', compact('barangs', 'kode', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kode_transaksi' => 'required|string|max:255',
            'tgl_pinjam' => 'required',
            'tgl_kembali' => 'required',
            'barang_id' => 'required',
            'pegawai_id' => 'required',

        ]);

        $transaksi = Transaksi::create([
                'kode_transaksi' => $request->get('kode_transaksi'),
                'tgl_pinjam' => $request->get('tgl_pinjam'),
                'tgl_kembali' => $request->get('tgl_kembali'),
                'barang_id' => $request->get('barang_id'),
                'pegawai_id' => $request->get('pegawai_id'),
                'ket' => $request->get('ket'),
                'status' => 'pinjam'
            ]);

        $transaksi->inventaris->where('id', $transaksi->barang_id)
                        ->update([
                            'jumlah' => ($transaksi->inventaris->jumlah - 1),
                            ]);

        return redirect()->route('transaksi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $data = Transaksi::findOrFail($id);


        if((Auth::user()->role == '3')) {
            return redirect()->to('/');
        }


        return view('transaksi.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaksi = Transaksi::find($id);

        $transaksi->update([    
                'status' => 'kembali'
                ]);

        $transaksi->inventaris->where('id', $transaksi->inventaris->id)
                        ->update([
                            'jumlah' => ($transaksi->inventaris->jumlah + 1),
                            ]);

        return redirect()->route('transaksi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Transaksi::find($id)->delete();
        return redirect()->route('transaksi.index');
    }
}
