<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */ 
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if ($user) {
            if ($user->role == 1) {
                return redirect('/dashboard');
            }elseif ($user->role == 2) {
                return redirect('/dashboard'); 
            }elseif ($user->role == 3) {
                    return redirect('/dashboard');
            }else {
                return view('login');
            }
        }
        else {
            return view('welcome'); 
        }
    }
}
