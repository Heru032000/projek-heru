<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class UserController extends Controller
{    
use RegistersUsers;
    protected $page = 'users';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $no = 1;
        $page=$this->page;
        return view($this->page.'/index',compact('users','no','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
<<<<<<< HEAD
    {
        $page = $this->page;
        return view($this->page.'/create',compact('page'));
=======
    { 
        $page = $this->page;
        return view($this->page.'/create',compact('page','users'));
>>>>>>> third commit
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     protected function validator(array $data)
     {
         return Validator::make($data, [
             'name' => ['required', 'string', 'max:30'],
             'email' => ['required', 'string', 'email', 'max:60', 'unique:users'],
             'password' => ['required', 'string', 'min:6', 'confirmed'],
             'nip' => ['required', 'string', 'max:20'],
             'alamat' => ['required', 'string', 'max:200'],
             'role' => ['required', 'string', 'max:200']
         ]);
     }

     
    public function store(Request $request)
    {
        $data=$request->all(); 
        
        if(count($request->name) > 0)
        {
            foreach($request->name as $item=>$v){
                $data2=array(
                    'name'=>$request->name[$item],
                    'email'=>$request->email[$item],
                    'nip'=>$request->nip[$item],
                    'alamat'=>$request->alamat[$item],
                    'role'=>$request->role[$item],
                    'password' => Hash::make($request->password[$item]),
                );
                User::insert($data2);
            }
        }
        return redirect('/admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user= User::find($id);
        $page = $this->page;
        return view($this->page.'/edit',compact('user','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        User::whereId($id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'nip' => $request->nip,
            'alamat' => $request->alamat,
            'password' => Hash::make($request['password']),
            'role' => $request->role,
        ]);

        return redirect('/admin/'.$this->page);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return redirect('/admin/users');
    }
}
