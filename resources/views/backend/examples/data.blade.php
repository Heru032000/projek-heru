@extends('layouts.app_admin')
@section('title')
    Inventaris | datatable
@endsection

@section('active-user')
  class="active"
@endsection

@section('content')

<div class="panel-header panel-header-lg">
  <canvas id="bigDashboardChart"></canvas>
</div>

<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title"> Contoh Tabel User</h4>
          <a href="{{url('#')}}" class="btn btn-round btn-primary">Tambah User</a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-borderless" id="table">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">First Name</th>
                        <th class="text-center">Last Name</th>
                        <th class="text-center">Email </th>
                        <th class="text-center">Gender</th>
                        <th class="text-center">Country</th>
                        <th class="text-center">Salary ($)</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $item)
                    <tr class="item{{$item->id}}">
                        <td>{{$item->id}}</td>
                        <td>{{$item->first_name}}</td>
                        <td>{{$item->last_name}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->gender}}</td>
                        <td>{{$item->country}}</td>
                        <td>{{$item->salary}}</td>
                        <td><button class="edit-modal btn btn-info"
                                data-info="{{$item->id}},{{$item->first_name}},{{$item->last_name}},{{$item->email}},{{$item->gender}},{{$item->country}},{{$item->salary}}">
                                <span class="glyphicon glyphicon-edit"></span> Edit
                            </button>
                            <button class="delete-modal btn btn-danger"
                                data-info="{{$item->id}},{{$item->first_name}},{{$item->last_name}},{{$item->email}},{{$item->gender}},{{$item->country}},{{$item->salary}}">
                                <span class="glyphicon glyphicon-trash"></span> Delete
                            </button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    $(document).ready(function() {
      $('#table').DataTable();
  } );
</script>
@endsection
