@extends('layouts.app_admin')
@section('title')
    Inventaris | Barang
@endsection
@section('active')
  class="active"
@endsection
@section('content')
<div class="panel-header panel-header-lg">
  <canvas id="bigDashboardChart"></canvas>
</div>
<div class="content">
  <div class="row"> 
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Tabel Barang</h4>
          <a href="{{url('#')}}" class="btn btn-round btn-primary">Tambah Barang</a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-borderless" id="table"> 
              <thead> 
                <tr> 
                  <th class = "text-center"> No </th> 
                  <th class = "text-center"> Nama Barang </th> 
                  <th class = "text-center"> Kode Barang </th> 
                  <th class = "text-center"> Kondisi </th> 
                  <th class = "text-center"> Keterangan </th> 
                  <th class = "text-center"> Jumlah </th> 
                  <th class = "text-center"> Jenis </th> 
                  <th class = "text-center"> Tanggal Masuk </th> 
                  <th class = "text-center"> Ruang </th> 
                  <th class = "text-center"> Pemasuk barang </th> 
                  <th class = "text-center"> Tindakan </th> 
                </tr> 
              </thead> 
              <tbody> 
                @foreach ($barang as $barang)
                <tr> 
                  <td> {{$no++}} </td> 
                  <td> {{$barang->nama}} </td> 
                  <td> {{$barang->kode_inventaris}} </td> 
                  <td> {{$barang->kondisi}} </td> 
                  <td> {{$barang->keterangan}} </td> 
                  <td> {{$barang->jumlah}} </td> 
                  <td> {{$barang->id_jenis}} </td> 
                  <td> {{$barang->tanggal_register}} </td> 
                  <td> {{$barang->id_ruang}} </td> 
                  <td> 
                    {!!$barang->role ==1 ? 'Admin' :''!!}
                    {!!$barang->role ==2 ? 'Operator' :''!!}
                    {!!$barang->role ==3 ? 'Peminjam' :''!!}
                  </td> 
                  <td><button class="edit-modal btn btn-info"
                    data-info="{{$barang->id}},{{$barang->nama}},{{$barang->kode_inventaris}},{{$barang->kondisi}},
                    {{$barang->keterangan}},{{$barang->jumlah}},{{$barang->keterangan}},{{$barang->role}}">
                    <span class="glyphicon glyphicon-edit"></span> Edit
                </button>
                <button class="delete-modal btn btn-danger"
                    data-info="{{$barang->id}},{{$barang->name}},{{$barang->email}},{{$barang->nip}},{{$barang->alamat}},{{$barang->role}}">
                    <span class="glyphicon glyphicon-trash"></span> Delete
                </button></td>
                </tr> 
                @endforeach
              </tbody> 
            </table> 
          </div>
        </div>
      </div> 
    </div>
  </div>
</div>
<script>
    $(document).ready(function() {
      $('#table').DataTable();
  } );
</script>
@endsection
