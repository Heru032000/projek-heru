<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    
    <!-- CSRF Token --> 
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>@yield('title')</title>
    
    @if (auth::user())
    
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    {{-- custom --}}
    <link href="../assets/css/custom.css" rel="stylesheet" />
    @if (auth::user()->role==3)
    <link href="../assets/css/custom_peminjaman.css" rel="stylesheet" />
    @endif
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="../assets/fontawesome/css/all.min.css" rel="stylesheet" />
    <!-- CSS Files -->
	<link rel="stylesheet" href="../assets/css/bootstrap336.css">
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../assets/css/now-ui-dashboard.css?v=1.2.0" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    {{-- <link href="../assets/demo/demo.css" rel="stylesheet" /> --}}
    {{-- data table --}}
    <script src="../assets/js/core/jquery.min.js"></script>
    {{-- <script src="../assets/js/jquery-1.12.3.js"></script> --}}
    <link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css">
    <!-- plugins:css --> 
    <link rel="stylesheet" href="{{asset('vendors/iconfonts/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/css/vendor.bundle.addons.css')}}">

    <!-- inject:css -->
    <!-- <link rel="stylesheet" href="{{asset('css/style.css')}}"> -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/select2.css')}}">
    <link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap4.min.css')}}">
    @section('css')

    @show
    <!-- endinject -->
</head>
<body class="">
    @if (auth::user()->role==1||auth::user()->role==2)
    <div class="wrapper ">  
        <div class="sidebar" data-color="red">
            <!--
                Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
            -->
            <div class="logo">
                <a href="{{url('/')}}" class="simple-text logo-mini">
                    DS
                </a>
                <a href="c" class="simple-text logo-normal">
                    Inventaris
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    @if (auth::user()->role==1||auth::user()->role==2||auth::user()->role==3)  
                    <li @yield('active')>
                        <a href="{{ url('/') }}">
                            <i class="fal fa-home"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li @yield('active-transaksi')>
                        <a href="{{url ('/transaksi')}}">
                            <i class="fal fa-person-carry"></i>
                            <p>Transaksi</p>
                        </a>
                    </li>
                    @endif
                    @if (auth::user()->role==1) 
                    <li>
                        <a data-toggle="collapse" href="#ui-master" aria-expanded="false" aria-controls="ui-master">
                            <i class="menu-icon mdi mdi-content-copy"></i>
                                <p>Master Data</p>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="ui-master">
                            <ul class="flex-column" style="list-style: none;">      
                                <li @yield('active-barang') style="padding-top: 20px;">
                                    <a href="{{ url('/admin/barang') }}">
                                        <i class="fal fa-boxes"></i>
                                        <p>Barang</p>
                                    </a>
                                </li>
                                <li @yield('active-jenis')>
                                    <a href="{{ url('/admin/jenis') }}">
                                        <i class="fal fa-atom"></i> 
                                        <p>Jenis Barang</p>
                                    </a>
                                </li>
                                <li @yield('active-ruang')>
                                    <a href="{{ url('/admin/ruang') }}">
                                        <i class="fal fa-door-open"></i>
                                        <p>Ruangan</p>
                                    </a>
                                </li>
                                <li @yield('active-user')>
                                    <a href="{{('/admin/users')}}">
                                        <i class="now-ui-icons users_single-02"></i>
                                        <p>User</p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    @endif 
                    @if (auth::user()->role==1)
                    <li>
                        <a data-toggle="collapse" href="#ui-laporan" aria-expanded="false" aria-controls="ui-laporan">
                            <i class="now-ui-icons menu-icon mdi mdi-table"></i>
                                <p>Laporan</p>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="ui-laporan">
                            <ul class="flex-column" style="list-style: none;">
                                <li style="padding-top: 20px;">
                                    <a href="{{url('admin/laporan/trs')}}">
                                        <i class="fal fa-person-carry"></i>                                    
                                        <p>Laporan Transaksi</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('admin/laporan/barang')}}">
                                        <i class="fal fa-boxes"></i>
                                       <p>Laporan Barang</p> 
                                    </a>
                                </li> 
                            </ul>
                        </div>
                    </li>
                    @endif
                </ul>
            </div>
            @endif
        </div>
        @endif
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg fixed-top navbar-transparent  bg-primary  navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        @if (auth::user()->role==1 || auth::user()->role==2)
                        <div class="navbar-toggle">
                            <button type="button" class="navbar-toggler">
                                <span class="navbar-toggler-bar bar1"></span>
                                <span class="navbar-toggler-bar bar2"></span>
                                <span class="navbar-toggler-bar bar3"></span>
                            </button>
                        </div>
                        @endif
                        <a class="navbar-brand" href="#pablo">@yield('navbar_name')</a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <ul class="navbar-nav">
                            <!-- Right Side Of Navbar -->
                            @guest
                            <li class="nav-item">
                                <a class="nav-link dropdown-toggle" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link dropdown-toggle" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                            @endif
                            @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                   Hello, {{ Auth::user()->name }}
                                </a>
                                
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        
        @yield('content')
        
        <footer class="footer">
            <div class="container">
                <nav>
                    <ul>
                        <li>
                            <a href="#">
                                Tugas Uji Kom SMKN 1 Ciomas
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright" id="copyright">
                    &copy;
                    <script>
                        document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
                    </script> Re Designed by
                    <a href="#" target="_blank">Heru</a>. Re Coded by
                    <a href="#" target="_blank">Heru</a>.
                </div>
            </div>
        </footer>
    </div>
</div>
{{-- data table --}}
<script src="../assets/js/jquery.dataTables.min.js"></script>
<script	src="../assets/js/dataTables.bootstrap.min.js"></script>
<!--   Core JS Files   -->
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap.min.js"></script>

<script src="../assets/js/plugins/perfect-scrollbar.jquery.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="../assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/now-ui-dashboard.min.js?v=1.2.0" type="text/javascript"></script>

<!-- HMM -->
  <script src="{{asset('vendors/js/vendor.bundle.addons.js')}}"></script>
  <script src="{{asset('js/off-canvas.js')}}"></script>
  <script src="{{asset('js/misc.js')}}"></script>
  <script src="{{asset('js/dashboard.js')}}"></script>
  <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{asset('js/sweetalert2.all.js')}}"></script>
  <script src="{{asset('js/select2.min.js')}}"></script>
  @section('js')

  @show

</body>
</html>