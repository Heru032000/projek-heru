@section('js')
<script type="text/javascript">
  $(document).on('click', '.pilih', function (e) {
    document.getElementById("barang_nama").value = $(this).attr('data-barang_nama');
    document.getElementById("barang_id").value = $(this).attr('data-barang_id');
    $('#myModal').modal('hide');
  });
  
  $(document).on('click', '.pilih_pegawai', function (e) {
    document.getElementById("pegawai_id").value = $(this).attr('data-pegawai_id');
    document.getElementById("pegawai_nama").value = $(this).attr('data-pegawai_name');
    $('#myModal2').modal('hide');
  });
  
  $(function () {
    $("#lookup, #lookup2").dataTable();
  });
  
</script>

@stop 
@section('css')

@stop
@extends('layouts.transaksi')
 
@section('content')

<form method="POST" action="{{ route('transaksi.store') }}" enctype="multipart/form-data">
  {{ csrf_field() }}
  <div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
      <div class="row flex-grow">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Tambah Transaksi baru</h4>
              
              <div class="form-group{{ $errors->has('kode_transaksi') ? ' has-error' : '' }}">
                <label for="kode_transaksi" class="col-md-4 control-label">Kode Transaksi</label>
                <div class="col-md-6">
                  <input id="kode_transaksi" type="text" class="form-control" name="kode_transaksi" value="{{ $kode }}" required readonly="">
                  @if ($errors->has('kode_transaksi'))
                  <span class="help-block">
                    <strong>{{ $errors->first('kode_transaksi') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('tgl_pinjam') ? ' has-error' : '' }}">
                <label for="tgl_pinjam" class="col-md-4 control-label">Tanggal Pinjam</label>
                <div class="col-md-3">
                  <input id="tgl_pinjam" type="date" class="form-control" name="tgl_pinjam" value="{{ date('Y-m-d', strtotime(Carbon\Carbon::today()->toDateString())) }}" required @if(Auth::user()->level == 'user') readonly @endif>
                  @if ($errors->has('tgl_pinjam'))
                  <span class="help-block">
                    <strong>{{ $errors->first('tgl_pinjam') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('tgl_kembali') ? ' has-error' : '' }}">
                <label for="tgl_kembali" class="col-md-4 control-label">Tanggal Kembali</label>
                <div class="col-md-3">
                  <input id="tgl_kembali" type="date"  class="form-control" name="tgl_kembali" value="{{ date('Y-m-d', strtotime(Carbon\Carbon::today()->addDays(5)->toDateString())) }}" required="" @if(Auth::user()->level == 'user') readonly @endif>
                  @if ($errors->has('tgl_kembali'))
                  <span class="help-block">
                    <strong>{{ $errors->first('tgl_kembali') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
              
              <div class="form-group{{ $errors->has('barang_id') ? ' has-error' : '' }}">
                <label for="barang_id" class="col-md-4 control-label">Barang</label>
                <div class="col-md-6">
                  <div class="input-group">
                    <input id="barang_nama" type="text" class="form-control" readonly="" required>
                    <input id="barang_id" type="hidden" name="barang_id" value="{{ old('barang_id') }}" required readonly="">
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-secondary" data-toggle="modal" data-target="#myModal"><b>Cari Barang</b> <span class="fa fa-search"></span></button>
                    </span>
                  </div>
                  @if ($errors->has('barang_id'))
                  <span class="help-block">
                    <strong>{{ $errors->first('barang_id') }}</strong>
                  </span>
                  @endif
                  
                </div>
              </div>
              
              
              @if(Auth::user()->role == '1'||Auth::user()->role == '2')
              <div class="form-group{{ $errors->has('pegawai_id') ? ' has-error' : '' }}">
                <label for="pegawai_id" class="col-md-4 control-label">Pegawai</label>
                <div class="col-md-6">
                  <div class="input-group">
                    <input id="pegawai_nama" type="text" class="form-control" readonly="" required>
                    <input id="pegawai_id" type="hidden" name="pegawai_id" value="{{ old('pegawai_id') }}" required readonly="">
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-warning btn-secondary" data-toggle="modal" data-target="#myModal2"><b>Cari pegawai</b> <span class="fa fa-search"></span></button>
                    </span>
                  </div>
                  @if ($errors->has('pegawai_id'))
                  <span class="help-block">
                    <strong>{{ $errors->first('pegawai_id') }}</strong>
                  </span>
                  @endif
                  
                </div>
              </div>
              @else
              <div class="form-group{{ $errors->has('pegawai_id') ? ' has-error' : '' }}">
                <label for="pegawai_id" class="col-md-4 control-label">Pegawai</label>
                <div class="col-md-6">
                  <input id="pegawai_nama" type="text" class="form-control" readonly="" value="{{Auth::user()->name}}" required>
                  <input id="pegawai_id" type="hidden" name="pegawai_id" value="{{ Auth::user()->id }}" required readonly="">
                  
                  @if ($errors->has('pegawai_id'))
                  <span class="help-block">
                    <strong>{{ $errors->first('pegawai_id') }}</strong>
                  </span>
                  @endif
                  
                </div>
              </div>
              @endif
              
              <div class="form-group{{ $errors->has('ket') ? ' has-error' : '' }}">
                <label for="ket" class="col-md-4 control-label">Keterangan</label>
                <div class="col-md-6">
                  <input id="ket" type="text" class="form-control" name="ket" value="{{ old('ket') }}">
                  @if ($errors->has('ket'))
                  <span class="help-block">
                    <strong>{{ $errors->first('ket') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
              
              <button type="submit" class="btn btn-primary" id="submit">
                Submit
              </button>
              <button type="reset" class="btn btn-danger">
                Reset
              </button>
              <a href="{{route('transaksi.index')}}" class="btn btn-light pull-right">Back</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</form>


<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-lg" role="document" >
    <div class="modal-content" style="background: #fff;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cari Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="lookup" class="table table-bordered table-hover table-striped">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Kode</th>
              <th>Kondisi</th>
              <th>Keterangan</th>
              <th>Stok</th>
            </tr>
          </thead>
          <tbody>
            @foreach($barangs as $data)
            <tr class="pilih" data-barang_id="<?php echo $data->id; ?>" data-barang_nama="<?php echo $data->nama; ?>" >
              <td>{{$data->nama}}</td>
                <td>{{$data->kode_inventaris}}</td>
                <td>{{$data->kondisi}}</td>
                <td>{{$data->keterangan}}</td>
                <td>{{$data->jumlah}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>  
        </div>
      </div>
    </div>
  </div>
  
  
  <!-- Modal -->
  <div class="modal fade bd-example-modal-lg" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document" >
      <div class="modal-content" style="background: #fff;">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cari pegawai</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <table id="lookup" class="table table-bordered table-hover table-striped">
            <thead>
              <tr>
                <th>
                  Nama
                </th>
                <th>
                  Nip 
                </th>
                <th>
                  Email
                </th>
                <th> 
                  Bagian
                </th>
              </tr>
            </thead>
            <tbody>
              @foreach($users as $data)
              <tr class="pilih_pegawai" data-pegawai_id="<?php echo $data->id; ?>" data-pegawai_name="<?php echo $data->name; ?>" >
                <td class="py-1">
                  {{$data->name}}
                </td>
                <td>
                  {{$data->nip}}
                </td>
                
                <td>
                  {{$data->email}}
                  
                </td>
                <td>
                  {{$data->role}}
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>  
        </div>
      </div>
    </div>
  </div>
  @endsection