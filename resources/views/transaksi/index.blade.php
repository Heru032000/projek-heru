@section('js')
@stop

@extends('layouts.app_admin')

@section('active-transaksi')
  class="active"
@endsection

@section('title')
Inventaris | Transaksi
@endsection

@if (auth::user()->role==1 || auth::user()->role==2)
  @section('navbar_name')
    Inventaris Transaksi
  @endsection
@endif

@if (auth::user()->role==3)
  @section('navbar_name')
    <a href="{{url('/dashboard')}}" class="btn btn-round btn-warning">Ke Halaman Dashboard</a>
  @endsection
@endif
 

@section('content')

<div class="panel-header panel-header-sm">
</div>

<div class="content"> 
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title"> Tabel Transaksi</h4>
          <a href="{{$page}}/create" class="btn btn-round btn-primary">Tambah Transaksi</a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-borderless" id="table"> 
              <thead> 
                <tr> 
                  <th class = "text-center"> Kode </th> 
                  <th class = "text-center"> Barang </th> 
                  <th class = "text-center"> Peminjam </th> 
                  <th class = "text-center"> Tgl Pinjam </th> 
                  <th class = "text-center"> Tgl Kembali </th> 
                  <th class = "text-center"> Status </th> 
                  <th class = "text-center"> Aksi </th> 
                </tr> 
              </thead>   
              <tbody> 
                @foreach ($datas as $data)
                <tr> 
                  <td> {{$data->kode_transaksi}} </td> 
                  <td> {{$data->inventaris->nama}} </td> 
                  <td> {{$data->user->name}} </td> 
                  <td> {{$data->tgl_pinjam}} </td> 
                  <td> {{$data->tgl_kembali}} </td> 
                  <td>
                      @if($data->status == 'pinjam')
                        <label class="badge badge-warning">Pinjam</label>
                      @else
                        <label class="badge badge-success">Kembali</label>
                      @endif
                      </td>
                      <td>
                      @if(Auth::user()->role == '1')
                      <div class="btn-group dropdown">
                      <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Action
                      </button>
                      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 30px, 0px);">
                      @if($data->status == 'pinjam')
                      <form action="{{ route('transaksi.update', $data->id) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('put') }}
<<<<<<< HEAD
                        <button class="dropdown-item" onclick="return confirm('Anda yakin data ini sudah kembali?')"> Sudah Kembali
=======
                        <button class="dropdown-item" onclick="return confirm('Anda yakin data ini Kembalikan?')"> Kembalikan
>>>>>>> third commit
                        </button>
                      </form>
                      @endif
                        <form action="{{ route('transaksi.destroy', $data->id) }}" class="pull-left"  method="post">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <button class="dropdown-item" onclick="return confirm('Anda yakin ingin menghapus data ini?')"> Delete
                        </button>
                      </form>
                      </div>
                    </div>
                    @endif

                    @if(Auth::user()->role == '2') 
                      @if($data->status == 'pinjam')
                        <form action="{{ route('transaksi.update', $data->id) }}" method="post" enctype="multipart/form-data">
                          {{ csrf_field() }}
                          {{ method_field('put') }}
<<<<<<< HEAD
                          <button class="btn btn-info btn-xs" onclick="return confirm('Anda yakin data ini sudah kembali?')">Sudah Kembali
=======
                          <button class="btn btn-info btn-xs" onclick="return confirm('Anda yakin data ini Kembalikan?')">Kembalikan
>>>>>>> third commit
                          </button>
                        </form>
                        @else
                        -
                      @endif
                    @endif
                    
                    @if(Auth::user()->role == '3')
                      @if($data->status == 'pinjam')
                        -
                        @else
                        -
                      @endif
                    @endif
                      </td>
                    </tr>
                  @endforeach
            </tbody> 
          </table> 
            </div>
            {{--  {!! $datas->links() !!} --}}
          </div>
        </div> 
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    $('#table').DataTable({
      "iDisplayLength": 10
    });
  });
</script>
@endsection
  