@extends('layouts.app_admin')
@section('title')
Inventaris | User
@endsection

@section('active-user')
class="active"
@endsection

@section('content')

<div class="panel-header panel-header-sm">
</div>

<div class="content"> 
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title"> Tabel User</h4>
          <a href="{{$page}}/create" class="btn btn-round btn-primary">Tambah User</a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-borderless" id="table"> 
              <thead> 
                <tr> 
                  <th class = "text-center"> No </th> 
                  <th class = "text-center"> Nama </th> 
                  <th class = "text-center"> Email </th> 
                  <th class = "text-center"> Nip </th> 
                  <th class = "text-center"> Alamat </th> 
                  <th class = "text-center"> Status </th> 
                  <th class = "text-center"> Tindakan </th> 
                </tr> 
              </thead> 
              <tbody> 
                @foreach ($users as $user)
                <tr> 
                  <td> {{$no++}} </td> 
                  <td> {{$user->name}} </td> 
                  <td> {{$user->email}} </td> 
                  <td> {{$user->nip}} </td> 
                  <td> {{$user->alamat}} </td> 
                  <td> 
                    {!!$user->role ==1 ? 'Admin' :''!!}
                    {!!$user->role ==2 ? 'Operator' :''!!}
                    {!!$user->role ==3 ? 'Peminjam' :''!!}
                  </td> 
                  <td>
                    <a href="{{$page.'/'.$user->id.'/edit'}}" class="btn btn-primary"><i class="fal fa-pen"></i></a>
                    <form action="users/{{$user->id}}" method="POST">
                      <input type="hidden" name="_method" value="DELETE">
                      @csrf
                      <button type="submit" class="btn btn-danger"><i class="fal fa-trash"></i></button>
                    </td>
                  </form>
                </tr>
                <!-- Modal -->
                {{-- <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> --}}
                        {{-- <h4 class="modal-title" id="myModalLabel">Edit Category</h4> --}}
                      {{-- </div>
                      <form action="/admin/{{$page}}/{{$user->id}}" method="post">
                        {{method_field('patch')}}
                        {{csrf_field()}}
                        <div class="modal-body">  
                          <input type="hidden" name="id" id="id" value="">
                          @include('users.form')
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Save Changes</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div> --}}
                {{-- akhir modal --}} 
                @endforeach
              </tbody> 
            </table> 
          </div>
        </div>
      </div> 
    </div>
  </div>
</div>

{{-- <script>
  $('#edit').on('show.bs.modal', function (event) {
    
    var button = $(event.relatedTarget) 
    var name = button.data('myname') 
    var password = button.data('mypassword') 
    var email = button.data('myemail')
    var nip = button.data('mynip') 
    var alamat = button.data('myalamat') 
    var role = button.data('myrole') 
    var modal = $(this)
    
    modal.find('.modal-body #name').val(name);
    modal.find('.modal-body #password').val(password);
    modal.find('.modal-body #email').val(email);
    modal.find('.modal-body #nip').val(nip);
    modal.find('.modal-body #alamat').val(alamat);
    modal.find('.modal-body #role').val(role);
  })
  
</script> --}}

<script>
  $(document).ready(function() {
    $('#table').DataTable();
  } );
</script>
@endsection
