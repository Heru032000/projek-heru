<!DOCTYPE html>
<html>
<head>
    <title>Multiple data send</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
</head>
<body>
    <div class="container">
        <br>
        @if(Session::has('success'))
        <div class="alert alert-success">
            {{Session::get('success')}}
        </div>
        @endif
        <form action="/admin/users" method="POST">
            {{csrf_field()}}
            <section>
                <div class="panel panel-header">
                    
                    <div class="row">
<<<<<<< HEAD
                            </div></div>
                            <div class="panel panel-footer" >
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Email</th>
                                            <th>Nip</th>
                                            <th>Alamat</th>
                                            <th>Bagian</th>
                                            <th>Password</th>
                                            <th><a href="#" class="addRow"><i class="glyphicon glyphicon-plus"></i></a></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><input type="text" name="name[]" class="form-control" required=""></td>
                                                <td><input type="email" name="email[]" class="form-control" required=""></td>    
                                                <td><input type="text" name="nip[]" class="form-control" required=""></td>    
                                                <td><input type="text" name="alamat[]" class="form-control" required=""></td>    
                                                <td><input type="text" name="role[]" class="form-control" required=""></td>    
                                                <td><input type="text" name="password[]" class="form-control" required=""></td>    
                                                <td><a href="#" class="btn btn-danger remove"><i class="glyphicon glyphicon-remove"></i></a></td>
                                            </tr>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td style="border: none"></td>
                                            <td style="border: none"></td>
                                            <td style="border: none"></td>
                                            <td style="border: none"></td>
                                            <td style="border: none"></td>
                                            <td><input type="submit" name="" value="Submit" class="btn btn-success"></td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <a href="{{route('users.index')}}" class="btn btn-warning pull-right">Back</a>
                            </div>
                        </section>
                    </form>
                </div>
                <script type="text/javascript">
                    $('tbody').delegate('.quantity,.budget','keyup',function(){
                        var tr=$(this).parent().parent();
                        var quantity=tr.find('.quantity').val();
                        var budget=tr.find('.budget').val();
                        var amount=(quantity*budget);
                        tr.find('.amount').val(amount);
                        total();
                    });
                    function total(){
                        var total=0;
                        $('.amount').each(function(i,e){
                            var amount=$(this).val()-0;
                            total +=amount;
                        });
                        $('.total').html(total+".00 tk");    
                    }
                    $('.addRow').on('click',function(){
                        addRow();
                    });
                    function addRow()
                    {
                        var tr='<tr>'+
                            '<td><input type="text" name="name[]" class="form-control" required=""></td>'+
                            '<td><input type="email" name="email[]" class="form-control" required=""></td>'+   
                            '<td><input type="text" name="nip[]" class="form-control" required=""></td>'+    
                            '<td><input type="text" name="alamat[]" class="form-control" required=""></td>'+    
                            '<td><input type="text" name="role[]" class="form-control" required=""></td>'+    
                            '<td><input type="text" name="password[]" class="form-control" required=""></td>'+    
                            '<td><a href="#" class="btn btn-danger remove"><i class="glyphicon glyphicon-remove"></i></a></td>'+
                            '</tr>';
                            $('tbody').append(tr);
                        };
                        $('.remove').live('click',function(){
                            var last=$('tbody tr').length;
                            if(last==1){
                                alert("you can not remove last row");
                            }
                            else{
                                $(this).parent().parent().remove();
                            }
                            
                        });
                    </script>
                </body>
                </html> 
=======
                    </div></div>
                    <div class="panel panel-footer" >
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Nip</th>
                                    <th>Alamat</th>
                                    <th>Bagian</th>
                                    <th>Password</th>
                                    <th><a href="#" class="addRow"><i class="glyphicon glyphicon-plus"></i></a></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="text" name="name[]" class="form-control" required=""></td>
                                    <td><input type="email" name="email[]" class="form-control" required=""></td>    
                                    <td><input type="text" name="nip[]" class="form-control" required=""></td>    
                                    <td><input type="text" name="alamat[]" class="form-control" required=""></td>    
                                    <td>
                                        <select class="form-control" name="role[]" required>
                                            <option value="">Silahkan Pilih</option>
                                            <option value="1">Admin</option>
                                            <option value="2">Operator</option>
                                            <option value="3">Peminjam</option>
                                        </select>
                                    </td>
                                    <td><input type="text" name="password[]" class="form-control" required=""></td>    
                                    <td><a href="#" class="btn btn-danger remove"><i class="glyphicon glyphicon-remove"></i></a></td>
                                </tr>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr> 
                                <td style="border: none"></td>
                                <td style="border: none"></td>
                                <td style="border: none"></td>
                                <td style="border: none"></td>
                                <td style="border: none"></td>
                                <td><input type="submit" name="" value="Submit" class="btn btn-success"></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <a href="{{route('users.index')}}" class="btn btn-warning pull-right">Back</a>
            </section>
        </form>
    </div>
    <script type="text/javascript">
        $('tbody').delegate('.quantity,.budget','keyup',function(){
            var tr=$(this).parent().parent();
            var quantity=tr.find('.quantity').val();
            var budget=tr.find('.budget').val();
            var amount=(quantity*budget);
            tr.find('.amount').val(amount);
            total();
        });
        function total(){
            var total=0;
            $('.amount').each(function(i,e){
                var amount=$(this).val()-0;
                total +=amount;
            });
            $('.total').html(total+".00 tk");    
        }
        $('.addRow').on('click',function(){
            addRow();
        });
        function addRow()
        {
            var tr='<tr>'+
                '<td><input type="text" name="name[]" class="form-control" required=""></td>'+
                '<td><input type="email" name="email[]" class="form-control" required=""></td>'+   
                '<td><input type="text" name="nip[]" class="form-control" required=""></td>'+    
                '<td><input type="text" name="alamat[]" class="form-control" required=""></td>'+    
                '<td><select class="form-control" name="role" required><option value="">Silahkan Pilih</option><option value="1">Admin</option><option value="2">Operator</option><option value="3">Peminjam</option></select></td>'+    
                '<td><input type="text" name="password[]" class="form-control" required=""></td>'+    
                '<td><a href="#" class="btn btn-danger remove"><i class="glyphicon glyphicon-remove"></i></a></td>'+
                '</tr>';
                $('tbody').append(tr);
            };
            $('.remove').live('click',function(){
                var last=$('tbody tr').length;
                if(last==1){
                    alert("you can not remove last row");
                }
                else{
                    $(this).parent().parent().remove();
                }
                
            });
        </script>
    </body>
    </html> 
>>>>>>> third commit
