@extends('layouts.transaksi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                {{-- <div class="panel-heading">Edit User</div> --}}

                <div class="panel-body">	
                <form action="/admin/{{$page}}/{{$user->id}}" method="post">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="name" class="form-control" value="{{$user->name}}">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" class="form-control" value="{{$user->email}}">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" name="alamat" class="form-control" value="{{$user->alamat}}">
                        </div>
                           <div class="form-group">
                            <label>Nip</label>
                            <input type="number" name="nip" class="form-control" value="{{$user->nip}}">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="text" name="password" class="form-control">
                        </div>
                        <div class="form-group"> 
                            <label>Bagian</label>
                        <select class="form-control" name="role" required>
                        <option value="">Silahkan Pilih</option>
                            <option {{$user->role == 1 ? 'selected':''}} value="1">Admin</option>
                            <option {{$user->role == 2 ? 'selected':''}} value="2">Operator</option>
                            <option {{$user->role == 3 ? 'selected':''}} value="3">Peminjam</option>
                        </select>
                        </div>

                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <a href="{{route('users.index')}}" class="btn btn-warning pull-right">Back</a>
                    </form>
	
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
