@extends('layouts.app_admin')

@section('title')
Inventaris | Jenis
@endsection

@section('active-jenis')
class="active"
@endsection

@section('content')

<div class="panel-header panel-header-sm">
</div>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> Contoh Tabel Jenis</h4>
                    <a href="{{$page}}/create" class="btn btn-round btn-primary">Tambah Jenis Barang</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-borderless" id="table"> 
                            <thead> 
                                <tr> 
                                    <th class = "text-center"> No </th> 
                                    <th class = "text-center"> Nama Jenis </th> 
                                    <th class = "text-center"> Kode Jenis </th> 
                                    <th class = "text-center"> Keterangan </th> 
                                    <th class = "text-center"> Tindakan </th> 
                                </tr> 
                            </thead> 
                            <tbody> 
                                @foreach ($jenis as $jenis)
                                <tr> 
                                    <td> {{$no++}} </td> 
                                    <td class = "text-center"> {{$jenis->nama_jenis}} </td> 
                                    <td class = "text-center"> {{$jenis->kode_jenis}} </td> 
                                    <td class = "text-center"> {{$jenis->keterangan}} </td>
                                    <td>
                                        <a href="{{$page.'/'.$jenis->id.'/edit'}}" class="btn btn-primary"><i class="fal fa-pen"></i></a>
                                        <form action="jenis/{{$jenis->id}}" method="POST">
                                            <input type="hidden" name="_method" value="DELETE">
                                            @csrf
                                            <button type="submit" class="btn btn-danger"><i class="fal fa-trash"></i></button>
                                        </td>
                                    </tr> 
                                </form>
                                @endforeach
                            </tbody> 
                        </table> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table').DataTable();
    } );
</script>
@endsection
