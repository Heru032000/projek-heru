@extends('layouts.transaksi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Ruang</div>

                <div class="panel-body">	
                <form action="/admin/{{$page}}/{{$jenis->id}}" method="post">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                        <div class="form-group">
                            <label>Nama Jenis</label>
                            <input type="text" name="nama_jenis" class="form-control" value="{{$jenis->nama_jenis}}">
                        </div>
                        <div class="form-group">
                            <label>Kode Ruang</label>
                            <input type="text" name="kode_jenis" class="form-control" value="{{$jenis->kode_jenis}}">
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" name="keterangan" class="form-control" value="{{$jenis->keterangan}}">
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <a href="{{route('jenis.index')}}" class="btn btn-warning pull-right">Back</a>
                    </form>
	
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
