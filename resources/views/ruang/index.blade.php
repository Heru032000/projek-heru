@extends('layouts.app_admin')
@section('title')
    Inventaris | Ruang
@endsection

@section('active-ruang')
  class="active"
@endsection
  
@section('content')
<div class="panel-header panel-header-sm">
</div>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title"> Contoh Tabel Ruang</h4>
          <a href="{{$page}}/create" class="btn btn-round btn-primary">Tambah Data Ruangan</a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-borderless" id = "table"> 
                  <thead> 
                    <tr> 
                      <th class = "text-center"> No </th> 
                      <th class = "text-center"> Nama Ruang </th> 
                      <th class = "text-center"> Kode Ruang </th> 
                      <th class = "text-center"> Keterangan </th> 
                      <th class = "text-center"> Tindakan </th> 
                    </tr> 
                  </thead>  
                  <tbody> 
                    @foreach ($ruang as $ruang)
                    <tr> 
                      <td> {{$no++}} </td> 
                      <td class = "text-center"> {{$ruang->nama_ruang}} </td> 
                      <td class = "text-center"> {{$ruang->kode_ruang}} </td> 
                      <td class = "text-center"> {{$ruang->keterangan}} </td>
                      <td>
                      <a href="{{$page.'/'.$ruang->id.'/edit'}}" class="btn btn-primary"><i class="fal fa-pen"></i></a>
                    <form action="ruang/{{$ruang->id}}" method="POST">
                      <input type="hidden" name="_method" value="DELETE">
                      @csrf
                      <button type="submit" class="btn btn-danger"><i class="fal fa-trash"></i></button>
                     </td>
                  </tr> 
                </form>
                    @endforeach
                  </tbody> 
              </table> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    $(document).ready(function() {
      $('#table').DataTable();
  } );
</script>
@endsection
