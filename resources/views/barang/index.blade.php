@extends('layouts.app_admin')
@section('title')
Inventaris | Barang
@endsection
@section('active-barang')
class="active"
@endsection
@section('content')
<div class="panel-header panel-header-sm">
</div>
<div class="content">
  <div class="row"> 
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Tabel Barang</h4>
          <a href="{{$page}}/create" class="btn btn-round btn-primary">Tambah Barang</a>
          <!-- Button trigger modal -->
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-borderless" id="table"> 
              <thead> 
                <tr> 
                  <th class = "text-center"> No </th> 
                  <th class = "text-center"> Nama Barang </th> 
                  <th class = "text-center"> Kode Barang </th> 
                  <th class = "text-center"> Kondisi </th> 
                  <th class = "text-center"> Keterangan </th> 
                  <th class = "text-center"> Jumlah </th> 
                  <th class = "text-center"> Jenis </th> 
                  <th class = "text-center"> Tanggal Masuk </th> 
                  <th class = "text-center"> Ruang </th> 
                  <th class = "text-center"> Pemasuk barang </th> 
                  <th class = "text-center"> Tindakan </th> 
                </tr> 
              </thead> 
              <tbody> 
                @foreach ($barang as $barang)
                <tr>  
                  <td> {{$no++}} </td> 
                  <td> {{$barang->nama}} </td> 
                  <td> {{$barang->kode_inventaris}} </td> 
                  <td> {{$barang->kondisi}} </td> 
                  <td> {{$barang->keterangan}} </td> 
                  <td> {{$barang->jumlah}} </td> 
                  <td> {{$barang->jenis->nama_jenis}} </td> 
                  <td> {{$barang->created_at}} </td> 
                  <td> {{$barang->ruang->nama_ruang}} </td> 
                  <td> 
                      {{$barang->user->name}}
                  </td> 
                  <td>
                    <a href="{{$page.'/'.$barang->id.'/edit'}}" class="btn btn-primary"><i class="fal fa-pen"></i></a>
                    <form action="barang/{{$barang->id}}" method="POST">
                      <input type="hidden" name="_method" value="DELETE">
                      @csrf
                      <button type="submit" class="btn btn-danger"><i class="fal fa-trash"></i></button>
                    </td>
                  </tr> 
                </form>
                @endforeach
              </tbody> 
            </table> 
          </div>
        </div>
      </div> 
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    $('#table').DataTable();
  } );
</script>
@endsection
