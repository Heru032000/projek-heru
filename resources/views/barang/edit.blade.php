@extends('layouts.transaksi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                {{-- <div class="panel-heading">Edit barang</div> --}}

                <div class="panel-body">	
                <form action="/admin/{{$page}}/{{$barang->id}}" method="post">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                        <div class="form-group">
                            <label>Nama Barang</label>
                            <input type="text" name="nama" class="form-control" value="{{$barang->nama}}">
                        </div>
                        <div class="form-group">
                            <label>Kode Inventaris</label>
                            <input type="text" name="kode_inventaris" class="form-control" value="{{$barang->kode_inventaris}}">
                        </div>
                        <div class="form-group">
                            <label>Kondisi</label>
                            <input type="text" name="kondisi" class="form-control" value="{{$barang->kondisi}}">
                        </div>
                           <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" name="keterangan" class="form-control" value="{{$barang->keterangan}}">
                        </div>
                        <div class="form-group">
                            <label>Jumlah</label>
                            <input type="text" name="jumlah" class="form-control" value="{{$barang->jumlah}}">
                        </div>
                        <div class="form-group">
                            <label>Jenis</label>
                            <input type="text" name="id_jenis" class="form-control" value="{{$barang->id_jenis}}">
                        </div>
                        <div class="form-group">
                            <label>Ruang</label>
                            <input type="text" name="id_ruang" class="form-control" value="{{$barang->id_ruang}}">
                    </div> 
                        <div class="form-group">
                            <label>Pemasok</label>
                            <select class="form-control" id="jenis" name="id_jenis[]" required>
                                <option value="">Silahkan Pilih</option>
                                @foreach($users as $user)
                                    <option value="{{$user->id}}"> {{$user->name}} </option>
                                @endforeach
                            </select>
                        </div>

                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <a href="{{route('barang.index')}}" class="btn btn-warning pull-right">Back</a>
                    </form>
	
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
